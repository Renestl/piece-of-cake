# "Oleald Free Website Template" Inspired Website

[Live demo](http://pieceocake.surge.sh/)

This project was inspired by [Oleald Free Website Template](http://www.os-templates.com/free-website-templates/oleald) (alt: http://www.os-templates.com/website-templates/template-demos/free-website-templates/oleald/)

No code or assets were used from the original. The original was used as inspiration. All code was written from scratch using HTML5, CSS3 and Sass.

## Credits

Photo by [Annie Spratt](https://unsplash.com/photos/6SHd7Q-l1UQ)
Photo by [Marcie Douglass](https://unsplash.com/photos/FqCB_SDXlmQ)
Photo by [Brooke Lark](https://unsplash.com/photos/V4MBq8kue3U)
Photo by [Alex Loup](https://unsplash.com/photos/On2VseHUDXw)
Photo by [lindsay Cotter](https://unsplash.com/photos/9J7sHieVFi0)
Photo by [Taylor Kiser](https://unsplash.com/photos/s7Vh1kg-clM)
Photo by [Sharon McCutcheon](https://unsplash.com/photos/s4RaGIo2eYI)
Photo by [Velizar Ivanov](https://unsplash.com/photos/rXB9YjOQX8I)
Photo by [Alexandra Gorn](https://unsplash.com/photos/QqfEYdWv-4U)
Photo by [Rodolfo Marques](https://unsplash.com/photos/kd6QWqXarJk)
Photo by [Viktor Forgacs](https://unsplash.com/photos/51AhxwkYyHo)
Photo by [Michał Parzuchowski](https://unsplash.com/photos/pMsvOrnIF3Y)
Photo by [Gianna Ciaramello](https://unsplash.com/photos/zXFBQL9MRZ8)
Photo by [Patricia Prudente](https://unsplash.com/photos/bS_Y6OqeUyc)

[Birthday Cake Color Palette](http://www.color-hex.com/color-palette/28766)

## Uses
* HTML
* CSS
* JavaScript