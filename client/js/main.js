$(document).ready(function () {
	
	$('#headerNav li').hover(
		function () {
			//show submenu
			$('ul', this).show();
	}, function () {
			//hide submenu
			$('ul', this).hide();
	});

	$("#toTop").click(function(event) {
		event.preventDefault();

		$("html, body").animate({ scrollTop: 0}, "slow");
		return false;
	});

});